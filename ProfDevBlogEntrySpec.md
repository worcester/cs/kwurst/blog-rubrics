# Specification for Self-Directed Professional Development Blog Entry

## Meets Specification

### ***All*** of the following must be true about student's blog entry to be considered to meet specification:

* is between 400 and 500 words in length.
* is posted by the due date.
* is written about a topic that directly relates to the course material, and that relationship is clear or clearly explained.
* contains a short summary of the selected blog post, podcast episode, article, book chapter, or other resource.
* contains the student's reason for selecting this particular resource - post, episode, etc.
* contains the student's comments on the content of the resource, backed up by personal reflection on the material, including what was learned, how the material affected the student and how the student expects to apply what was learned in their future practice.
* contains a link to the resource (if it is online) or a citation if it from a print source or database.
* contains the `CS@Worcester` tag so that it is syndicated to the CS@Worcester blog.
* contains the tag for the course number.
* contains the tag for the week number e.g `Week-1` (see the list of due dates for the correspondence between week numbers and due dates.)

## Does Not Yet Meet Specification

### If ***any*** of the following are true about the student's blog entry it is considered to not meet specification:

* The link or citation for the resource is missing.
* The link or citation for the resource is to a website, rather than the specific post or episode.
* The blog entry is not tagged correctly.
* The blog entry is simply a summary of the resource with no reflection on why it was selected, what the student learned, how the student was affected by the resource, or how the student expects to apply what was learned in the future.
* The content of the blog post is not clearly related to the course material.

&copy; 2021 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
