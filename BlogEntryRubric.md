# Blog Entry Rubric

Level of Mastery (Score)

 | Excellent (4) |	Good (3) | Satisfactory (2) | Needs Improvement (1) | Unsatisfactory (0)
---|---|---|---|---|---
Content |	Blog entry contains reflection on what the student learned from the week's activities, how the student would proceed differently in light of this, and how what was learned might be applied in other situations. Includes all material from level 3 as well. | Blog entry contains an explanation of why the student took the steps s/he did during the week. Describes what was tried, what succeeded and what failed, and how the student chose the course of action. Includes references to where information/help was found. Includes all material from level 2 as well. | Blog entry contains detailed description of what was done during the week. Provides a list of steps that could be followed by another person to do the same thing. | Blog entry contains only a cursory description of what was done during the week. | No blog entry posted for the week. 

##Copyright and License
####&copy; 2014 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
