# Blog Entry Rubrics

Collection of rubrics to be used to score student blog entries.

- [Self-Directed Professional Development Blog Entry](./ProfDevBlogEntrySpec.md) - To be used for student blog posts about self-directed reading into course topics.
- [Blog Entry](./BlogEntryRubric.md) - Outdated rubric for students posting about weekly learning.

&copy; 2022 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.